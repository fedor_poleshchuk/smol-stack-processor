#ifndef TRANSLATOR_H
#define TRANSLATOR_H

#include <map>
#include <stack>
#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <stddef.h>
#include <stdint.h>

#include "structs.h"

class Translator
{
    std::fstream file_stream;
    size_t command_index;

    std::map<std::string, size_t> word_map;
    std::stack<std::pair<size_t, size_t>> command_index_stack;
    std::vector<Command> command_vector;

    bool is_special_word(std::string word);

    Command tryParseAsSimpleCommand(std::string word);
    Command tryParseAsSequence(std::string word);
    Command tryParseAsConditional(std::string word);
    Command tryParseAsLoop(std::string word);
    Command tryParseAsProcedure(std::string word);

public:
    Translator();
    Translator(std::string file);

    void open(std::string file);
    void compile();
    void emit_code(std::string dst);
};

#endif // TRANSLATOR_H
