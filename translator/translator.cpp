#include "translator.h"

bool Translator::is_special_word(std::string word)
{
    if(word == "then" ||
       word == "begin")
        return true;

    return false;
}

Command Translator::tryParseAsSimpleCommand(std::string word)
{
    Command command;
    if(word == "+")
        command.command = "add";
    else if(word == "-")
        command.command = "sub";
    else if(word == "*")
        command.command = "mul";
    else if(word == "/")
        command.command = "div";
    else if(word == "%")
        command.command = "mod";
    else if(word == "=")
        command.command = "eq";
    else if(word == "r@")
        command.command = "r@";
    else if(word == "r>")
        command.command = "r>";
    else if(word == ">r")
        command.command = ">r";
    else if(word == "!")
        command.command = "store";
    else if(word == "@")
        command.command = "load";
    else if(word == "and")
        command.command = "and";
    else if(word == "or")
        command.command = "or";
    else if(word == "not")
        command.command = "not";
    else if(word == "drop")
        command.command = "drop";
    else if(word == "swap")
        command.command = "swap";
    else if(word == "dup")
        command.command = "dup";
    else if(word == "rot")
        command.command = "rot";
    else if(word == "over")
        command.command = "over";
    else if(word == "in")
        command.command = "in";
    else if(word == "out")
        command.command = "out";
    else if(word == "<")
        command.command = "lt";
    else if(word == ">")
        command.command = "gt";
    else
    {
        command.command = "";
    }

    return command;
}

Command Translator::tryParseAsSequence(std::string word)
{
    Command command;

    command = tryParseAsSimpleCommand(word);
    if(!command.command.empty())
        return command;

    command = tryParseAsConditional(word);
    if(!command.command.empty())
        return command;

    command = tryParseAsLoop(word);
    if(!command.command.empty())
        return command;

    command = tryParseAsProcedure(word);
    if(!command.command.empty())
        return command;

    if(command.command.empty() && !is_special_word(word)) // then ничего не делает
    {
        command.command = "push";
        command.opt_op1 = word;
        command_index++;
    }
    return command;
}

Command Translator::tryParseAsConditional(std::string word)
{
    Command command;
    if(word == "if")
    {
        command.command = "?branch";
        command_index_stack.push(std::make_pair(command_vector.size(), command_index));
    }
    else if(word == "else")
    {
        auto if_index = command_index_stack.top();
        std::stringstream ss; ss << command_index + 2;
        command_vector[if_index.first].opt_op1 = ss.str();
        command_index_stack.pop();

        command.command = "branch";
        command_index_stack.push(std::make_pair(command_vector.size(), command_index));
        command_index++;
    }
    else if(word == "then")
    {
        auto else_index = command_index_stack.top();
        std::stringstream ss; ss << command_index;
        command_vector[else_index.first].opt_op1 = ss.str();
        command_index_stack.pop();
    }
    return command;
}

Command Translator::tryParseAsLoop(std::string word)
{
    Command command;
    if(word == "begin")
    {
        command_index_stack.push(std::make_pair(command_vector.size(), command_index));
    }
    else if(word == "while")
    {
        command.command = "?branch";
        command_index_stack.push(std::make_pair(command_vector.size(), command_index));
    }
    else if(word == "repeat")
    {
        auto while_index = command_index_stack.top();
        std::stringstream ss; ss << command_index + 2;
        command_vector[while_index.first].opt_op1 = ss.str();
        command_index++;
        command_index_stack.pop();

        auto begin_index = command_index_stack.top();
        std::stringstream ss2; ss2 << begin_index.second - 1;
        command.command = "branch";
        command.opt_op1 = ss2.str();
        command_index++;
        command_index_stack.pop();
    }

    return command;
}

Command Translator::tryParseAsProcedure(std::string word)
{
    return Command();
}

Translator::Translator()
{
    command_index = 0;
}

Translator::Translator(std::string file)
{
    command_index = 0;
    file_stream.open(file);
}

void Translator::open(std::string file)
{
    file_stream.open(file);
}

void Translator::compile()
{
    std::string word;

    // как же я себя за это ненавижу...
    while(file_stream >> word)
    {
        Command command;
        command = tryParseAsSequence(word);

        //std::cout << command.toString() << " " << command_index << std::endl;

        if(!is_special_word(word))
        {
            command_vector.push_back(command);
            command_index++;
        }
    }

    for(auto command:command_vector)
    {
        std::cout << command.toString() << std::endl;
    }

    Command halt_command = {"halt"};
    command_vector.push_back(halt_command);
}

void Translator::emit_code(std::string dst)
{
    std::ofstream out(dst);
    for(auto command:command_vector)
    {
        out << command.toString();
    }
    out.close();
}


