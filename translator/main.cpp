#include <QCoreApplication>

#include "translator.h"

/*
    Использование -- translator <файл с текстом> [<конечный файл>]
*/

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    std::string ifile = "";
    std::string ofile = "";

    if(argc == 2)
    {
        ifile.assign(argv[1]);
        ofile.assign("a.out");
    }
    if(argc == 3)
    {
        ifile.assign(argv[1]);
        ofile.assign(argv[2]);
    }

    Translator t(ifile);
    t.compile();
    t.emit_code(a.applicationDirPath().toStdString() + "/" + ofile);

    return a.exec();
}
