#ifndef STRUCTS_H
#define STRUCTS_H

#include <sstream>
#include <stddef.h>
#include <stdint.h>
#include <string>

struct Command
{
    std::string command = "";
    std::string opt_op1 = "";
    std::string opt_op2 = "";

    Command() = default;
    Command(std::string comm) { command = comm; }

    void from_word(std::string word)
    {
        word.erase(word.begin());
        word.erase(word.end() - 1);

        std::stringstream ss(word);
        ss >> command;
        ss >> opt_op1;
        ss >> opt_op2;
    }

    std::string toString()
    {
        std::stringstream ss;
        ss << "[" << command;
        if(!opt_op1.empty())
            ss << " " << opt_op1 ;
        if(!opt_op2.empty())
            ss << " " << opt_op2 ;
        ss << "]";

        return ss.str();
    }
};

#endif // STRUCTS_H
