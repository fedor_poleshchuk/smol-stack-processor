#include "executor.h"

Executor::value_type Executor::getTopValue(bool should_pop)
{
    if(val_stack.size() == 0)
        throw std::out_of_range("FATAL: stack underflow, terminating...");

    int64_t val;
    val = val_stack.top();

    if(should_pop)
        val_stack.pop();

    return val;
}

void Executor::evalCommand(Command command)
{
    std::cout << "PC = " << program_counter << " : " << command.command << std::endl;
    if(command.command == "push")
    {
        program_counter++;
        int64_t val = stoi(memory.at(program_counter));
        val_stack.push(val);
    }
    else if(command.command == "add")
    {
        int64_t val1, val2;
        val1 = getTopValue();
        val2 = getTopValue();
        val_stack.push(val1 + val2);
    }
    else if(command.command == "sub")
    {
        int64_t val1, val2;
        val1 = getTopValue();
        val2 = getTopValue();
        val_stack.push(val1 - val2);
    }
    else if(command.command == "mul")
    {
        int64_t val1, val2;
        val1 = getTopValue();
        val2 = getTopValue();
        val_stack.push(val1 * val2);
    }
    else if(command.command == "div")
    {
        int64_t val1, val2;
        val1 = getTopValue();
        val2 = getTopValue();
        val_stack.push(val1 / val2);
    }
    else if(command.command == "mod")
    {
        int64_t val1, val2;
        val1 = getTopValue();
        val2 = getTopValue();
        val_stack.push(val1 % val2);
    }
    else if(command.command == "eq")
    {
        int64_t val1, val2;
        val1 = getTopValue();
        val2 = getTopValue();
        val_stack.push(val1 == val2);
    }
    else if(command.command == "lt")
    {
        int64_t val1, val2;
        val1 = getTopValue();
        val2 = getTopValue();
        val_stack.push(val1 < val2);
    }
    else if(command.command == "gt")
    {
        int64_t val1, val2;
        val1 = getTopValue();
        val2 = getTopValue();
        val_stack.push(val1 > val2);
    }
    else if(command.command == "le")
    {
        int64_t val1, val2;
        val1 = getTopValue();
        val2 = getTopValue();
        val_stack.push(val1 <= val2);
    }
    else if(command.command == "ge")
    {
        int64_t val1, val2;
        val1 = getTopValue();
        val2 = getTopValue();
        val_stack.push(val1 >= val2);
    }
    else if(command.command == "and")
    {
        int64_t val1, val2;
        val1 = getTopValue();
        val2 = getTopValue();
        val_stack.push(val1 and val2);
    }
    else if(command.command == "or")
    {
        int64_t val1, val2;
        val1 = getTopValue();
        val2 = getTopValue();
        val_stack.push(val1 or val2);
    }
    else if(command.command == "not")
    {
        int64_t val1;
        val1 = getTopValue();
        val_stack.push(!val1);
    }
    else if(command.command == "drop")
    {
        val_stack.pop();
    }
    else if(command.command == "swap")
    {
        int64_t val1, val2;
        val1 = getTopValue();
        val2 = getTopValue();
        val_stack.push(val1);
        val_stack.push(val2);
    }
    else if(command.command == "dup")
    {
        val_stack.push(val_stack.top());
    }
    else if(command.command == "rot")
    {
        int64_t val1, val2, val3;
        val1 = getTopValue();
        val2 = getTopValue();
        val3 = getTopValue();
        val_stack.push(val2);
        val_stack.push(val1);
        val_stack.push(val3);
    }
    else if(command.command == "over")
    {
        int64_t val1, val2;
        val1 = getTopValue();
        val2 = getTopValue();
        val_stack.push(val2);
        val_stack.push(val1);
        val_stack.push(val2);
    }
    else if(command.command == "branch")
    {
        program_counter++;
        int64_t val = stoi(memory.at(program_counter));
        program_counter = val;
    }
    else if(command.command == "?branch")
    {
        int64_t val = getTopValue();
        program_counter++;
        if(val == 0)
        {
            int64_t val = stoi(memory.at(program_counter));
            program_counter = val;
        }
    }
    else if(command.command == "in")
    {
        value_type port = getTopValue();
        std::cout << "on port " << port << " awaiting input > ";

        value_type val = 0; std::cin >> val;
        val_stack.push(val);
    }
    else if(command.command == "out")
    {
        value_type port = getTopValue();
        std::cout << "on port " << port << " output data: " << val_stack.top() << std::endl;
        val_stack.pop();
    }
    else if(command.command == "store")
    {
        value_type address = getTopValue();
        std::stringstream ss; ss << val_stack.top();
        memory[address] = ss.str();
        val_stack.pop();
    }
    else if(command.command == "load")
    {
        value_type address = getTopValue();
        val_stack.push(stoi(memory[address]));
    }
}

Executor::Executor(std::string filename)
{
    program_counter = 0;
    file_stream.open(filename);
}

void Executor::upload()
{
    value_type index = 0;
    while(true)
    {
        std::string word;
        word = from_line();

        if(word.empty())
            break;

        Command command;
        command.from_word(word);
        memory.emplace(index, command.command); index++;
        if(!command.opt_op1.empty()) {memory.emplace(index, command.opt_op1); index++;}
        if(!command.opt_op2.empty()) {memory.emplace(index, command.opt_op2); index++;}
    }
}

std::string Executor::from_line()
{
    std::string word;

    do
    {
        auto character = file_stream.get();
        if(character == -1)
            break;

        word.push_back(character);
    } while(word.back() != ']');

    return word;
}

void Executor::eval()
{
    try
    {
        while(true)
        {
            std::string command = memory.at(program_counter);

            if(command == "halt")
            {
                std::cout << "HALT: successfully terminated" << std::endl;
                return;
            }
            else
                evalCommand(command);

            program_counter++;
        }
    }
    catch (std::out_of_range e)
    {
        std::cerr << e.what() << std::endl;
        return;
    }
    catch (...)
    {
        std::cerr << "FATAL unexpected error while running program; terminating..." << std::endl;
        return;
    }
}
