#ifndef EXECUTOR_H
#define EXECUTOR_H

#include <map>
#include <stack>
#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <stddef.h>
#include <stdint.h>

#include "structs.h"

class Executor
{
    typedef int64_t value_type;

    std::fstream file_stream;

    // это память, но не память
    std::map<value_type, std::string> memory;
    size_t program_counter;

    std::stack<value_type> val_stack;
    std::stack<value_type> ret_stack;

    std::string from_line();
    value_type getTopValue(bool should_pop = true);

    void evalCommand(Command command);

public:
    Executor(std::string filename);

    void upload();
    void eval();
};

#endif // EXECUTOR_H
