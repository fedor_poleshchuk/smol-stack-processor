#include <QCoreApplication>

#include "executor.h"

/*
    Использование -- executor <файл с текстом>
*/

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    std::string ifile = "";

    if(argc == 2)
    {
        ifile.assign(argv[1]);
    }

    Executor e(ifile);
    e.upload();
    e.eval();

    return a.exec();
}
